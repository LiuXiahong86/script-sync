#1.开始时log
Write-Output "[$(Get-Date -Format "yyyy.MM.dd HH:MM:ss")][DownloadPNG] DownloadPNG.ps1 started successfully" | Out-File -Append .\Record.txt
Write-Output "[$(Get-Date -Format "yyyy.MM.dd HH:MM:ss")][DownloadPNG] DownloadPNG.ps1 started successfully"
#2. 获取PNG列表
$Content=ConvertFrom-Json -InputObject (Get-Content -Raw  -Path "..\BackgroundLink.json")
#3. 下载PNG
mkdir "D:\Pictures\background-sync"
for ($x = 0; $x -lt 1; $x++) {
    $FileName=($Content[$x].Split("/"))[-1]
    Write-Output "[$(Get-Date -Format "yyyy.MM.dd HH:MM:ss")][DownloadPNG] Downloading No.$($x+1) File : $($Content[$x])" | Out-File -Append .\Record.txt
    Write-Output "[$(Get-Date -Format "yyyy.MM.dd HH:MM:ss")][DownloadPNG] Downloading No.$($x+1) File : $($Content[$x])"
    Invoke-WebRequest -Uri $Content[$x] -OutFile "D:\Pictures\background-sync\$FileName"
}
#4. 结束时log
Write-Output "[$(Get-Date -Format "yyyy.MM.dd HH:MM:ss")][DownloadPNG] DownloadPNG.ps1 has finished running" | Out-File -Append .\Record.txt
Write-Output "[$(Get-Date -Format "yyyy.MM.dd HH:MM:ss")][DownloadPNG] DownloadPNG.ps1 has finished running"